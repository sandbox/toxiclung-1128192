CREATE TABLE IF NOT EXISTS `sms_core` (
  `uid` int(16) NOT NULL,
  `number` varchar(256) CHARACTER SET utf8 NOT NULL,
  `code` int(128) NOT NULL,
  `confirm` int(1) NOT NULL
);

 
CREATE TABLE IF NOT EXISTS `sms_journal` (
  `uid` int(8) NOT NULL,
  `text` varchar(128) CHARACTER SET utf8 NOT NULL,
  `time` int(16) NOT NULL,
  `send` int(8) NOT NULL
);


CREATE TABLE IF NOT EXISTS `sms_subc` (
  `uid` int(8) NOT NULL,
  `suid` int(8) NOT NULL
);

CREATE TABLE IF NOT EXISTS `sms_subc_node` (
  `uid` int(8) NOT NULL,
  `node` int(8) NOT NULL,
  `comment` int(8) NOT NULL,
  `allnode` int(8) NOT NULL
);